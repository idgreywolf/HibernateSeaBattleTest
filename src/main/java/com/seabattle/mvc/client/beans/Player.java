package com.seabattle.mvc.client.beans;


import com.seabattle.mvc.client.services.FieldRandomGenerator;

/**
 * ����� ������
 */
public class Player {
    private String playerName;
    private Field playerField;

    public void init(){
        playerField=new Field();
        setRandomField();
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }



    public Field getPlayerField() {
        return playerField;
    }

    public void setPlayerField(Field playerField) {
        this.playerField = playerField;
    }


    public void setRandomField(){
        playerField.clear();
        FieldRandomGenerator frg=new FieldRandomGenerator();
        playerField=frg.generateRandomField();
    }


}
