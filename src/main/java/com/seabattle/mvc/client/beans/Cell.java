package com.seabattle.mvc.client.beans;

import java.io.Serializable;

//����� ������, ������ ��������� � ������ �� �������, ���� �� � ��� ����
public class Cell implements Serializable{
    private int state;// ���������, 0-�� ��������, 1-����, 2-�����, 3-����
    private Ship ship;//�������, ���� ���� � ���� ������

    public int getState() {
        return state;
    }

    public void setState(byte state) {
        this.state = state;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
