package com.seabattle.mvc.client.beans;

import java.io.Serializable;
import java.util.HashMap;

public class Ship implements Serializable{

    private int size;//������ �������
    private HashMap<Integer,Boolean> deckStates;//��������� ����� ������� true - �����, false - ��������
    private boolean alive;//����� ��������� ������� true - ���, false - ����

    public int getSize(){return size;}

    public void setSize(int size){
        this.size=size;
        deckStates=new HashMap<>();
        for (int i = 1; i <=size; i++) {
            deckStates.put(i,true);
        }
    }

    public boolean isAlive() {
        return alive;
    }

    private boolean checkDecks(){
        for (int i = 1; i <=size ; i++)
            if (!deckStates.get(i))
                return false;
        return true;
    }
}