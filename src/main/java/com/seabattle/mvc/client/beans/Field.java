package com.seabattle.mvc.client.beans;

import java.io.Serializable;

//����� ���� ������
public class Field implements Serializable{

    private Cell field[][]=new Cell[10][10];

    public Field(){
        for (int i=0;i<10;i++)
            for (int j=0;j<10;j++){
                field[i][j]=new Cell();
            }
    }

    public void clear(){
        for (int i=0;i<10;i++)
            for (int j=0;j<10;j++){
                field[i][j]=new Cell();
            }
    }

    //������� ��������� ������ �� �������� �����������
    public boolean isPutable(int x,int y){

        if (!isInBorders(x,y)) return false;
        if (isInBorders(x,y)&&!isFree(x,y)) return false;

        if (isInBorders(x-1,y-1)&&!isFree(x - 1, y - 1)) return false;
        if (isInBorders(x-1,y)&&!isFree(x-1,y)) return false;
        if (isInBorders(x-1,y+1)&&!isFree(x - 1, y + 1)) return false;
        if (isInBorders(x,y-1)&&!isFree(x, y - 1)) return false;
        if (isInBorders(x,y+1)&&!isFree(x, y + 1)) return false;
        if (isInBorders(x+1,y-1)&&!isFree(x + 1, y - 1)) return false;
        if (isInBorders(x+1,y)&&!isFree(x + 1, y)) return false;
        if (isInBorders(x+1,y+1) &&!isFree(x+1,y+1)) return false;

        return true;
    }

    public Cell getCell(int x, int y) {
        return field[y][x];
    }

    public void setCell(int x,int y,Cell newCell){
        field[y][x]=newCell;
    }

    //�������� �� ������
    private boolean isFree(int x, int y){
        return (getCell(x, y).getShip() == null);
    }


    //��������� �� ���������� � �������� ����
    private boolean isInBorders(int x, int y){
        return ((x > 0) && (x < 10) && (y > 0) && (y < 10));
    }

    public Cell[][] getField(){
        return field;
    }

}