package com.seabattle.mvc.client.services;

import com.seabattle.mvc.client.beans.Field;
import com.seabattle.mvc.client.beans.Ship;

import java.util.Random;

/**
 * ��������� ��������� �����
 */
public class FieldRandomGenerator {

    private Field randomField;


    //��������� ���������� �������� �� ����
    public Field generateRandomField(){
        randomField=new Field();
        createRandomShip(4);
        createRandomShip(3);createRandomShip(3);
        for (int i = 0; i < 3; i++) createRandomShip(2);
        for (int i = 0; i < 4; i++) createRandomShip(1);
        return randomField;
    }

    //����� ���������� � � �������� �����������
    private int moveX(int x, int direction){
        switch (direction) {
            case 1: {
                x += 1;
                break;
            }
            case 3: {
                x -= 1;
                break;
            }
        }
        return x;
    }

    //����� ���������� � � �������� �����������
    private int moveY(int y, int direction){
        switch (direction) {
            case 0: {
                y -= 1;
                break;
            }
            case 2: {
                y += 1;
                break;
            }
        }
        return y;
    }


    //��������� ������� �� ���� �� �������� ����������� � �����������
    private void placeShip(Ship ship,int baseX,int baseY,int direction){

        int nextX=baseX;
        int nextY=baseY;
        randomField.getCell(baseX,baseY).setShip(ship);
        for (int j = 0; j < ship.getSize()-1; j++) {
            nextX=moveX(nextX,direction);
            nextY=moveY(nextY, direction);
            randomField.getCell(nextX, nextY).setShip(ship);
        }

    }

    //�������� ������� ��������� ������� �� ��������� ����������� � �����������
    private void createRandomShip(int size){
        Ship newShip= new Ship();
        newShip.setSize(size);
        Random rnd=new Random();
        int direction=rnd.nextInt(4),baseX,baseY;
        boolean isDone=false;
        do {
            baseX=rnd.nextInt(10);
            baseY=rnd.nextInt(10);
            if (!randomField.isPutable(baseX, baseY)) continue;
            for (int i = 0; i < 4; i++) {
                isDone=true;
                int nextX=baseX;
                int nextY=baseY;
                for (int j = 0; j < size-1; j++) {
                    nextX=moveX(nextX,direction);
                    nextY=moveY(nextY,direction);
                    if (!randomField.isPutable(nextX,nextY)) {
                        isDone = false;
                        break;
                    }
                }
                if (isDone) break;
                direction = ((direction + 1) > 3) ? 0 : direction++;
            }
        }while (!isDone);
        placeShip(newShip,baseX,baseY,direction);
    }
}
