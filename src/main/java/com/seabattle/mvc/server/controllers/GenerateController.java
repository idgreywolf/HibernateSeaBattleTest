package com.seabattle.mvc.server.controllers;

import com.seabattle.mvc.client.beans.Player;
import com.seabattle.mvc.server.domains.PlayerEntity;
import com.seabattle.mvc.server.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;


@Controller
@RequestMapping("/generate")
public class GenerateController {

	@Autowired
	PlayerService playerService;
	@Resource (name="player1")
	Player player1;
	@Resource (name="player2")
	Player player2;
	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(@RequestParam("player1name")String player1name,@RequestParam("player2name")String player2name, ModelMap model) {
		player1.setPlayerName(player1name);
		player2.setPlayerName(player2name);
		player1.setRandomField();
		player2.setRandomField();

		playerService.insertPlayerEntity(new PlayerEntity(player1.getPlayerName(), player1.getPlayerField()));
		playerService.insertPlayerEntity(new PlayerEntity(player2.getPlayerName(),player2.getPlayerField()));

		model.addAttribute("player1name", player1.getPlayerName());
		model.addAttribute("player1field",player1.getPlayerField());
		model.addAttribute("player2name", player2.getPlayerName());
		model.addAttribute("player2field",player2.getPlayerField());
		return "generate";
	}
}