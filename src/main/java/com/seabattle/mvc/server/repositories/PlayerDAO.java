package com.seabattle.mvc.server.repositories;

import com.seabattle.mvc.server.domains.PlayerEntity;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ���� on 28.06.2015.
 */
@Repository
public class PlayerDAO {
    @Autowired
    SessionFactory sessionFactory;
    @Transactional
    public void insertPlayerEntity(PlayerEntity playerEntity){
        sessionFactory.getCurrentSession().persist(playerEntity);
    }
}
