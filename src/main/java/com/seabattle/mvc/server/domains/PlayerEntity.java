package com.seabattle.mvc.server.domains;

import com.seabattle.mvc.client.beans.Cell;
import com.seabattle.mvc.client.beans.Field;

import javax.persistence.*;

/**
 * Created by ���� on 28.06.2015.
 */
@Entity
@Table
public class PlayerEntity {
    @Id
    @GeneratedValue
    @Column
    private int id;
    @Column
    private String playerName;
    @Column
    private Field field;

    public PlayerEntity() {
    }

    public PlayerEntity(String playerName,Field field) {
        this.field = field;
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public Field getField() {
        return field;
    }

    public void setField(Field field) {
        this.field = field;
    }
}
