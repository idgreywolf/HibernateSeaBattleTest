package com.seabattle.mvc.server.services;

import com.seabattle.mvc.server.domains.PlayerEntity;
import com.seabattle.mvc.server.repositories.PlayerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by ���� on 28.06.2015.
 */
@Service
public class PlayerService {
    @Autowired
    PlayerDAO playerDAO;

    public void insertPlayerEntity(PlayerEntity playerEntity){
        playerDAO.insertPlayerEntity(playerEntity);
    }
}
